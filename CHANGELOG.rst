^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package ailibot2
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.3 (2023-08-24)
* libgazebo_ros_planar_move.so 插件不能发布base_link到轮之间的变换的，导致rviz上的模型显示报错。但不影响控制O3 ,O4, m4控制
* 修改模型，增加发布 joint_state_publisher ，设置publish_wheel_tf为false，TF树才完整
* 整合 robot_localization fuse odom + imu 

1.0.2 (2023-08-22)
* add ailibot2_slam ailibot2_nav ailibot2_bringup
* gmapping done
* slam_toolbox done 
* sudo apt install ros-humble-rmw-cyclonedds-cpp  change to cyclonedds
* export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp  to  ~/.bashrc


1.0.1 (2023-08-21)
-------------------
* rviz test pass 
* gazebo test pass 

1.0.0 (2023-08-17)
-------------------
* init ailibot2_sim for ros2 humble
* add ailibot2 d2 d4 o3 o4 m4 model
 