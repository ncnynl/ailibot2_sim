from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from os.path import join
from ament_index_python.packages import get_package_share_directory
from launch.conditions import IfCondition

def generate_launch_description():

    #timeout 
    save_map_timeout = LaunchConfiguration('save_map_timeout', default='10000.00')
    #map path and map name 
    #default path is install/ailibot2_nav/map/map.yaml
    my_map_file  = [get_package_share_directory('ailibot2_nav'), "/map/map.yaml"]

    return LaunchDescription([

        DeclareLaunchArgument(
            'map',
            default_value=my_map_file,
            description='[localize] Full path to map yaml file to load'),

        Node(
            package='nav2_map_server', 
            executable='map_saver_cli', 
            output='screen', 
            parameters=[{'save_map_timeout': save_map_timeout}],
            arguments=['-f', LaunchConfiguration('map')],
        )
    ])
