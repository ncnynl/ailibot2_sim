from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from os.path import join
from ament_index_python.packages import get_package_share_directory
from launch.conditions import IfCondition

def generate_launch_description():
    # paths
    robot_description_path = get_package_share_directory("ailibot2_slam")
    slam_toolbox_config_path = join(
        robot_description_path,
        "config",
        "slam_toolbox_mapping.yaml"
    )

    use_sim_time = LaunchConfiguration('use_sim_time', default='True')
    use_rviz = LaunchConfiguration('use_rviz', default='false')
    rviz_config_dir = join(get_package_share_directory('ailibot2_slam'),
                                   'rviz', 'slam_toolbox.rviz')

    return LaunchDescription([

        Node(
            parameters=[
                slam_toolbox_config_path,
                {"use_sim_time": use_sim_time}
            ],
            package="slam_toolbox",
            executable="async_slam_toolbox_node",
            name="slam_toolbox_localization",
            output="screen"
        ),
        
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', rviz_config_dir],
            parameters=[{'use_sim_time': use_sim_time}],
            condition=IfCondition(use_rviz),
            output='screen'),
    ])
