#!/usr/bin/env python3

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch.substitutions import ThisLaunchFileDir
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():

    use_sim_time = LaunchConfiguration('use_sim_time', default='true')
    pkg_bringup_ros = FindPackageShare(package='ailibot2_bringup').find('ailibot2_bringup') 
    pkg_gazebo_ros = FindPackageShare(package='ailibot2_gazebo').find('ailibot2_gazebo') 

    robot_type = LaunchConfiguration('robot_type', default='d2')
    model_type = LaunchConfiguration('model_type', default='d2')
    world_type = LaunchConfiguration('world_type', default="basic")


    return LaunchDescription([
        DeclareLaunchArgument(
            'use_sim_time',
            default_value=use_sim_time,
            description='Use simulation (Gazebo) clock if true'),

        DeclareLaunchArgument(
            name='robot_type',
            default_value=robot_type,
            description='robot type to load'),

        DeclareLaunchArgument(
            name='world_type',
            default_value=world_type,
            description='world type to load'),

        # raw_odom
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(os.path.join(pkg_gazebo_ros, 'launch', 'load_sdf_into_gazebo_with_param.launch.py')),
            launch_arguments={'world_type': world_type, "robot_type": robot_type, "model_type": model_type}.items()
        ),

        # # odom (fuse raw_odom and imu) 
        # IncludeLaunchDescription(
        #     PythonLaunchDescriptionSource(os.path.join(pkg_bringup_ros, 'launch', 'ekf.launch.py'))
        # ) 
    ])
