#!/usr/bin/env python3

import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.substitutions import Command, LaunchConfiguration, PythonExpression

def generate_launch_description():

    rviz_config_dir = [get_package_share_directory('ailibot2_description'), '/rviz/']
    rviz_config_dir.append(LaunchConfiguration('rviz_type', default='odom'))
    rviz_config_dir.append(".rviz") 

    # rviz_config_dir = os.path.join(
    #     get_package_share_directory('ailibot2_description'),
    #     'rviz',
    #     'model.rviz')

    return LaunchDescription([
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', rviz_config_dir],
            output='screen'),
    ])