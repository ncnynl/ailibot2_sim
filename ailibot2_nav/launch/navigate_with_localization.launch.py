import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare
from launch.conditions import IfCondition, UnlessCondition

def generate_launch_description():
    package_name = 'ailibot2_nav'

    use_sim_time = LaunchConfiguration('use_sim_time', default='True')
    use_rviz = LaunchConfiguration('use_rviz')
    pkg_share = FindPackageShare(package=package_name).find(package_name)

    static_map_path = [get_package_share_directory('ailibot2_nav'), '/map/map.yaml']
    map_file = LaunchConfiguration('map', default=static_map_path)
    
    nav2_params_path = [get_package_share_directory('ailibot2_nav'), '/param/']
    nav2_params_path.append(LaunchConfiguration('nav2_params', default='nav2_params'))
    nav2_params_path.append(".yaml")


    nav2_launch_file_dir = os.path.join(get_package_share_directory('ailibot2_nav'), 'launch')

    rviz_config_dir = os.path.join(
        get_package_share_directory('ailibot2_nav'),
        'rviz',
        'navigation2.rviz')

    return LaunchDescription([
        DeclareLaunchArgument(
            'map',
            default_value=static_map_path,
            description='Full path to map file to load'),

        DeclareLaunchArgument(
            'params_file',
            default_value=nav2_params_path,
            description='Full path to param file to load'),

        DeclareLaunchArgument(
            'use_sim_time',
            default_value='false',
            description='Use simulation (Gazebo) clock if true'),

        DeclareLaunchArgument(
            name='use_rviz',
            default_value='True',
            description='Whether to start RVIZ'),            

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([nav2_launch_file_dir, '/bringup_with_localization.launch.py']),
            launch_arguments={
                'map': map_file,
                'use_sim_time': use_sim_time,
                'params_file': nav2_params_path}.items(),
        ),
        Node(
            condition=IfCondition(use_rviz),
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', rviz_config_dir],
            parameters=[{'use_sim_time': use_sim_time}],
            output='screen'),
    ])
