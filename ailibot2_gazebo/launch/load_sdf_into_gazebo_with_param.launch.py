import os
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess, IncludeLaunchDescription
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import Command, LaunchConfiguration, PythonExpression
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare
from ament_index_python.packages import get_package_share_directory
from osrf_pycommon.terminal_color import ansi



def generate_launch_description():

  # Constants for paths to different files and folders
  package_name = 'ailibot2_gazebo'
  robot_name_in_model = 'ailibot2'

  # sdf_model_path = 'models/ailibot2/model.sdf'
  sdf_model_path = [get_package_share_directory('ailibot2_gazebo'), '/models/ailibot2/']
  sdf_model_path.append(LaunchConfiguration('robot_type', default='d2'))
  sdf_model_path.append("/ailibot_")
  sdf_model_path.append(LaunchConfiguration('model_type', default='d2'))
  sdf_model_path.append(".sdf")  

  default_urdf_model_path = [get_package_share_directory('ailibot2_description'), '/urdf/']
  default_urdf_model_path.append(LaunchConfiguration('robot_type', default='d2'))
  default_urdf_model_path.append("/ailibot_")
  default_urdf_model_path.append(LaunchConfiguration('model_type', default='d2'))
  default_urdf_model_path.append(".urdf.xacro")   

  # sdf_model_path = 'models/turtlebot3/model.sdf'
  # world_file_path = 'worlds/empty.world'
  world_path = [get_package_share_directory('ailibot2_gazebo'), '/worlds/']
  world_path.append(LaunchConfiguration('world_type', default='basic'))
  world_path.append(".world")    
    
  # Pose where we want to spawn the robot
  spawn_x_val = '0.0'
  spawn_y_val = '0.0'
  spawn_z_val = '0.0'
  spawn_yaw_val = '0.0'

  ############ You do not need to change anything below this line #############
  
  # Set the path to different files and folders.  
  pkg_gazebo_ros = FindPackageShare(package='gazebo_ros').find('gazebo_ros')   
  pkg_share = FindPackageShare(package=package_name).find(package_name)
  # world_path = os.path.join(pkg_gazebo_ros, world_file_path)
  # gazebo_models_path = os.path.join(pkg_share, gazebo_models_path)
  # os.environ["GAZEBO_MODEL_PATH"] = gazebo_models_path
  # sdf_model_path = os.path.join(pkg_share, sdf_model_path)

  ailibot2_models_path = os.path.join(get_package_share_directory('ailibot2_description'), 'meshes')
  gazebo_models_path = os.path.join(ailibot2_models_path)
  if 'GAZEBO_MODEL_PATH' in os.environ:
      os.environ['GAZEBO_MODEL_PATH'] += ":" + gazebo_models_path
  else :
      os.environ['GAZEBO_MODEL_PATH'] = gazebo_models_path
  print(ansi("yellow"), "GAZEBO_MODEL_PATH: ",os.environ['GAZEBO_MODEL_PATH'], ansi("reset"))

  # Launch configuration variables specific to simulation
  gui = LaunchConfiguration('gui')
  headless = LaunchConfiguration('headless')
  namespace = LaunchConfiguration('namespace')
  sdf_model = LaunchConfiguration('sdf_model')
  urdf_model = LaunchConfiguration('urdf_model')
  use_namespace = LaunchConfiguration('use_namespace')
  use_sim_time = LaunchConfiguration('use_sim_time')
  use_simulator = LaunchConfiguration('use_simulator')
  world = LaunchConfiguration('world')
  
  # Map fully qualified names to relative ones so the node's namespace can be prepended.
  # In case of the transforms (tf), currently, there doesn't seem to be a better alternative
  # https://github.com/ros/geometry2/issues/32
  # https://github.com/ros/robot_state_publisher/pull/30
  # TODO(orduno) Substitute with `PushNodeRemapping`
  #              https://github.com/ros2/launch_ros/issues/56
  remappings = [('/tf', 'tf'),
                ('/tf_static', 'tf_static')]

  # Declare the launch arguments  
  declare_namespace_cmd = DeclareLaunchArgument(
    name='namespace',
    default_value='',
    description='Top-level namespace')

  declare_use_namespace_cmd = DeclareLaunchArgument(
    name='use_namespace',
    default_value='false',
    description='Whether to apply a namespace to the navigation stack')
            
  declare_sdf_model_path_cmd = DeclareLaunchArgument(
    name='sdf_model', 
    default_value=sdf_model_path, 
    description='Absolute path to robot sdf file')

  declare_urdf_model_path_cmd = DeclareLaunchArgument(
    name='urdf_model', 
    default_value=default_urdf_model_path, 
    description='Absolute path to robot urdf file')

  declare_simulator_cmd = DeclareLaunchArgument(
    name='headless',
    default_value='False',
    description='Whether to execute gzclient')
    
  declare_use_sim_time_cmd = DeclareLaunchArgument(
    name='use_sim_time',
    default_value='true',
    description='Use simulation (Gazebo) clock if true')

  declare_use_simulator_cmd = DeclareLaunchArgument(
    name='use_simulator',
    default_value='True',
    description='Whether to start the simulator')

  declare_world_cmd = DeclareLaunchArgument(
    name='world',
    default_value=world_path,
    description='Full path to the world model file to load')
  
  # Start Gazebo server
  start_gazebo_server_cmd = IncludeLaunchDescription(
    PythonLaunchDescriptionSource(os.path.join(pkg_gazebo_ros, 'launch', 'gzserver.launch.py')),
    condition=IfCondition(use_simulator),
    launch_arguments={'world': world}.items())

  # Start Gazebo client    
  start_gazebo_client_cmd = IncludeLaunchDescription(
    PythonLaunchDescriptionSource(os.path.join(pkg_gazebo_ros, 'launch', 'gzclient.launch.py')),
    condition=IfCondition(PythonExpression([use_simulator, ' and not ', headless])))

  # Launch the robot
  spawn_entity_cmd = Node(
    package='gazebo_ros', 
    executable='spawn_entity.py',
    arguments=['-entity', robot_name_in_model, 
               '-file', sdf_model,
                  '-x', spawn_x_val,
                  '-y', spawn_y_val,
                  '-z', spawn_z_val,
                  '-Y', spawn_yaw_val],
                  output='screen')

  # Subscribe to the joint states of the robot, and publish the 3D pose of each link.
  start_robot_state_publisher_cmd = Node(
    # condition=IfCondition(use_robot_state_pub),
    package='robot_state_publisher',
    executable='robot_state_publisher',
    namespace=namespace,
    parameters=[{'use_sim_time': use_sim_time, 'robot_description': Command(['xacro ', urdf_model])}],
    remappings=remappings,
    arguments=[default_urdf_model_path])

  # start_static_transform_publisher = Node(
  #   package='tf2_ros',
  #   executable='static_transform_publisher',
  #   arguments = ["0", "0", "0", "0", "0", "0", "base_footprint", "base_link"])

  # Create the launch description and populate
  ld = LaunchDescription()

  # Declare the launch options
  ld.add_action(declare_namespace_cmd)
  ld.add_action(declare_use_namespace_cmd)
  ld.add_action(declare_sdf_model_path_cmd)
  ld.add_action(declare_urdf_model_path_cmd)
  ld.add_action(start_robot_state_publisher_cmd)    
  ld.add_action(declare_simulator_cmd)
  ld.add_action(declare_use_sim_time_cmd)
  ld.add_action(declare_use_simulator_cmd)
  ld.add_action(declare_world_cmd)

  # Add any actions
  ld.add_action(start_gazebo_server_cmd)
  ld.add_action(start_gazebo_client_cmd)
  ld.add_action(spawn_entity_cmd)
  # ld.add_action(start_static_transform_publisher)

  return ld
