# ailibot2_sim

ailibot2_sim ailibot2 ros2版本仿真包

#article
* 目录
https://www.ncnynl.com/category/ailibot2-sim/
* 制作模型，参考ailibot模型入门教程
* 制作场景，参考ailibot1仿真教程
* 通过场景制作地图，参考ailibot1仿真教程
* 查看模型
* 查看模型，可以设置参数
* 加载场景到gazebo,可以设置参数
* 加载URDF模型到gazebo,可以设置参数
* 加载SDF模型到gazebo,可以设置参数
* 启动rviz和gazebo都使用URDF模型的仿真
* 启动rviz使用URDF模型，gazebo使用SDF模型的仿真
* 启动rviz和gazebo都使用URDF模型,融合robot_localization包的仿真
* 加载rviz配置,可以设置参数
* 键盘控制
* wheel/odometry和odom效果比较
* gmapping建图，保存地图，可设置参数
* slam_toolbox建图 ，保存地图，可设置参数
* cartographer建图 ，保存地图，可设置参数
* 启动导航，边建图边导航，可以设置参数
* 启动导航，使用现成地图导航，可指定地图

#快捷安装（推荐）
```
#1.安装RCM工具/ROS1/2命令管理器
```
curl -k https://www.ncnynl.com/rcm.sh | bash -
```

#2.执行安装命令，部署脚本，也包含安装其他依赖包
rcm ros2_ailibot2 install_ros2_ailibot2_sim
```

#一步步安装
```
#1.先安装依赖包
sudo apt-get install ros-${ROS_DISTRO}-teleop-twist-keyboard 
sudo apt-get install ros-${ROS_DISTRO}-navigation2 ros-${ROS_DISTRO}-urdf  ros-${ROS_DISTRO}-xacro 
sudo apt-get install ros-${ROS_DISTRO}-compressed-image-transport ros-${ROS_DISTRO}-rqt-tf-tree
sudo apt-get install ros-${ROS_DISTRO}-slam-toolbox

#2.安装算法包
cartographer，gmapping

#3.ailibot2仿真安装流程
$ mkdir -p ~/ros2_ailibot2_sim_ws/src
$ cd ~/ros2_ailibot2_sim_ws/src
$ git clone https://gitee.com/ncnynl/ailibot2_sim
$ cd ~/ros2_ailibot2_sim_ws
$ colcon build --symlink-install
$ source ~/ros2_ailibot2_sim_ws/install/local_setup.bash
```

#启动rviz和gazebo都使用URDF模型的仿真

```
ros2 launch ailibot2_bringup robot_sim.launch.py 
#可选参数
robot_type:=d2/d4/m4/o3/o4
model_type:=d2/d4/m4/o3/o4
```

#启动rviz使用URDF模型，gazebo使用SDF模型的仿真
```
ros2 launch ailibot2_bringup robot_sim_with_sdf.launch.py
#可选参数
robot_type:=ailibot2
model_type:=d2
```

#启动rviz和gazebo都使用URDF模型,融合robot_localization包的仿真
```
ros2 launch ailibot2_bringup robot_sim_with_ekf.launch.py
#可选参数
robot_type:=d2/d4/m4/o3/o4
model_type:=d2/d4/m4/o3/o4
```

#比较wheel/odometry和odom
```
ros2 launch ailibot2_bringup rviz2_odom.launch.py
```

#加载rviz配置,可以设置参数
```
ros2 launch ailibot2_bringup rviz2.launch.py
可选参数： 
rviz_type:=model或odom
```

#查看模型
```
ros2 launch ailibot2_description load_urdf_to_rviz.launch.py 
ros2 launch ailibot2_description display.launch.py 
```

#查看模型，可以设置参数
```
ros2 launch ailibot2_description display_with_param.launch.py 
#可选参数
robot_type:=d2/d4/m4/o3/o4
model_type:=d2/d4/m4/o3/o4
```

#加载场景到gazebo
```
ros2 launch ailibot2_gazebo load_world_into_gazebo.launch.py
```

#加载URDF模型到gazebo
```
ros2 launch ailibot2_gazebo load_urdf_into_gazebo.launch.py
```

#加载URDF模型到gazebo,可以设置参数
```
ros2 launch ailibot2_gazebo load_urdf_into_gazebo_with_param.launch.py
#可选参数
robot_type:=d2/d4/m4/o3/o4
model_type:=d2/d4/m4/o3/o4
```

#加载SDF模型到gazebo
```
ros2 launch ailibot2_gazebo load_sdf_into_gazebo.launch.py
```

#加载SDF模型到gazebo,可以设置参数
```
ros2 launch ailibot2_gazebo load_sdf_into_gazebo.launch.py
#可选参数
robot_type:=ailibot2
model_type:=d2
```

#gmapping建图，可设置参数
```
ros2 launch ailibot2_slam gmapping.launch.py
#可选参数
use_rviz:=true  同时可打开rviz
```

#slam_toolbox建图 ，可设置参数
```
ros2 launch ailibot2_slam slam_toolbox.launch.py
#可选参数
use_rviz:=true  同时可打开rviz
```

#cartographer建图 ，可设置参数
```
ros2 launch ailibot2_slam cartographer.launch.py
#可选参数
use_rviz:=true  同时可打开rviz
```


#启动导航，边建图边导航，可以设置参数
```
ros2 launch ailibot2_nav navigate_with_slam.launch.py
#可选参数
nav2_params:=nav2_params 不同导航参数文件
use_rviz:=true  同时可打开rviz
```

#启动导航，使用现成地图导航
```
ros2 launch ailibot2_nav navigate_with_localization.launch.py
#可选参数
map:=${HOME}/map/map.yaml
nav2_params:=nav2_params 不同导航参数文件
use_rviz:=true  同时可打开rviz
```

#键盘控制
```
ros2 launch ailibot2_teleop keyboard.launch.py  默认方式
ros2 launch ailibot2_teleop keyboard_tb3.launch.py   turtlebot3键盘控制方式
```

#保存地图
```
mkdir -p ~/map #建立地图目录
ros2 run nav2_map_server map_saver_cli -f ~/map/map --ros-args -p save_map_timeout:=10000.00
ros2 launch ailibot2_slam save_map.launch.py map:=${HOME}/map/map.yaml
```