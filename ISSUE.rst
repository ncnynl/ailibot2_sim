^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
ISSUE for package ailibot2
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2023-08-23
* robot_localization Failed to meet update rate! Try decreasing the rate, limiting sensor output frequency, or limiting the number of sensors.
https://answers.ros.org/question/285601/robot_localization-asking-for-map-to-odom-transform/

* Message Filter dropping message: frame 'laser_frame' at time 1640913301.637 for reason 'the timestamp on the message is earlier than all the data in the transform cache'
https://answers.ros.org/question/393581/for-nav2-lidar-timestamp-on-the-message-is-earlier-than-all-the-data-in-the-transform-cache/

2023-08-22 

* [slam_toolbox]: Message Filter dropping message: for reason 'discarding message because the queue is full' 
https://answers.ros.org/question/389383/slam_toolbox-message-filter-dropping-message-for-reason-discarding-message-because-the-queue-is-full/

2023-08-21

* libgazebo_ros_planar_move.so 插件不能发布base_link到轮之间的变换的，导致rviz上的模型显示报错。但不影响控制O3 ,O4, m4控制
* m4 模型太大，需要压缩，使用Ubuntu meshlab stl压缩: Filters->remeshing*** -> Simplificaition: ***Decimation, setting target number 
* EasyInstallDeprecationWarning: easy_install command is deprecated. Use build and pip and other standards-based tools.
https://github.com/open-mmlab/OpenPCDet/issues/782
https://answers.ros.org/question/396439/setuptoolsdeprecationwarning-setuppy-install-is-deprecated-use-build-and-pip-and-other-standards-based-tools/


2023-08-17
-------------------
* Link inertia error should tell which link has inertia error

* Warning: TF_OLD_DATA ignoring data from the past for frame drivewhl_l_link at time 9523.654000 according to authority Authority undetectable
https://github.com/ros2/rviz/issues/872

* Warning: Invalid frame ID "drivewhl_r_link" passed to canTransform argument source_frame - frame does not exist
https://answers.ros.org/question/382196/urdf-joint-type-continuous-causes-rviz2-issues/


* The root link base_link has an inertia specified in the URDF, but KDL does not support a root link with an inertia.  As a workaround, you can add an extra dummy link to your URDF.
https://answers.ros.org/question/192817/error-msg-the-root-link_base-has-an-inertia-specified-in-the-urdf-but-kdl/

* spawn_entity.py cannot publish URDF meshes to Gazebo11
https://github.com/ros-simulation/gazebo_ros_pkgs/issues/1272

TO URDF , mesh need to file://
<mesh filename="file://$(find ailibot2_description)/meshes/ailibot2/robot_base.stl" />

TO SDF , mesh need to  package://
